Unity Version: 2020.3.15f2
Packages Used: 
-Input System 1.1.1
-Cinemachine 2.6.11


Enemy Patrol Script:
Turning 'patrolToPoint' bool to true makes enemy run to attached child gameObject position and back to his starting position.
Turning 'stationary' bool to true will prevent enemy from moving.
Turning 'fall off platforms' bool to true will prevent enemy from turning when he reaches edge of the platform.

Additional Content:
-Double Jump
-Health Potions
-Tips (Button layout, explaining coins, energy)
-Spirit Ball ability and Energy System
-Combo System (Killing enemies before timer runs out fills out your combo meter)
-Grace Period for some actions (For example pressing jump briefly before hitting the ground will still register button 
as pressed and execute the action. Without this controls feel more clunky because player can miss his input by few frames)